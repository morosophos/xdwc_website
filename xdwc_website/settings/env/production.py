import os
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

DEBUG = False
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ['SECRET_KEY']

ALLOWED_HOSTS = ['xdwc.teichisma.info', 'xdwc.xdf.gg']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ['DB_NAME'],
    }
}

sentry_sdk.init(
    dsn=os.environ['SENTRY_DSN'],
    traces_sample_rate=1.0,
    # integrations=[DjangoIntegration()]
)
