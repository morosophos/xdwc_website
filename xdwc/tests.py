from django.test import TestCase

from xdwc.eesystem_utils import calculate_c2


class EesystemTestCase(TestCase):
    def test_calculate_c2(self):
        self.assertEquals(calculate_c2(1), 99)
        self.assertEquals(calculate_c2(2), 98)
        self.assertEquals(calculate_c2(5), 95)
        self.assertEquals(calculate_c2(10), 90)
        self.assertEquals(calculate_c2(13), 87)
        self.assertEquals(calculate_c2(49), 51)
        self.assertEquals(calculate_c2(50), 50)
        self.assertEquals(calculate_c2(51), 49.5)
        self.assertEquals(calculate_c2(52), 49)
        self.assertEquals(calculate_c2(60), 45)
        self.assertEquals(calculate_c2(100), 25)
        self.assertEquals(calculate_c2(101), 24.75)
        self.assertEquals(calculate_c2(102), 24.5)
