from decimal import Decimal

import re
from xon_db import XonoticDB

from .models import TimeRecord, SpeedRecord


class BrokenDBError(Exception):
    pass


class ServerDB:
    def __init__(self, data):
        self.db = XonoticDB(data)

    def uid2name(self, crypto_idfp):
        try:
            return self.db[f'/uid2name/{crypto_idfp}']
        except KeyError:
            raise BrokenDBError('{crypto_idfp} not preset in uid2name')

    def process_map(self, server, map):
        self.process_times(server, map)
        self.process_speed(server, map)

    def process_times(self, server, map):
        map_name = map.xonotic_name
        position_re = re.compile(f'{map_name}/cts100record/crypto_idfp(\d+)')
        something_new = False
        for k, crypto_idfp in self.db.filter(position_re, is_regex=True):
            match = position_re.match(k)
            pos = match.group(1)
            time = self.db.get(f'{map_name}/cts100record/time{pos}')
            if time is None:
                raise BrokenDBError(f'No time for position {pos} on map {map_name}')
            time = Decimal(time) / 100
            nickname = self.uid2name(crypto_idfp)
            new_time_record = TimeRecord.objects.add_new_record(
                server=server,
                map=map,
                crypto_idfp=crypto_idfp,
                nickname=nickname,
                time=time
            )
            something_new = something_new or (new_time_record is not None)
        return something_new

    def process_speed(self, server, map):
        map_name = map.xonotic_name
        crypto_idfp = self.db.get(f'{map_name}/cts100record/speed/crypto_idfp')
        speed = self.db.get(f'{map_name}/cts100record/speed/speed')
        something_new = False
        if crypto_idfp and speed:
            nickname = self.uid2name(crypto_idfp)
            speed = Decimal(speed)
            new_speed_record = SpeedRecord.objects.add_new_record(
                server=server,
                map=map,
                crypto_idfp=crypto_idfp,
                nickname=nickname,
                speed=speed
            )
            something_new = something_new or (new_speed_record is not None)
        return something_new
