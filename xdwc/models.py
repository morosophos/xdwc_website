import decimal
import math
import os
import time
from enum import Enum

from django_enum_choices.fields import EnumChoiceField
import logging
import requests
from decimal import Decimal
from django.conf import settings
from django.db import models
from django.utils import timezone
from djrichtextfield.models import RichTextField
from ordered_model.models import OrderedModel

from xdwc.eesystem_utils import calculate_c2
from xdwc.util import strip_colors, html_colors, qfont_decode

logger = logging.getLogger(__name__)


class ScoringSystem(Enum):
    ORIGINAL_EESYSTEM = 'original_eesystem'
    REVISED_EESYSTEM = 'revised_eesystem'


class Tournament(models.Model):
    name = models.CharField(max_length=512)
    slug = models.SlugField(db_index=True)
    published = models.BooleanField(default=False)
    scoring_system = EnumChoiceField(ScoringSystem, default=ScoringSystem.ORIGINAL_EESYSTEM)
    is_default = models.BooleanField(default=False)
    standings_last_updated_at = models.DateTimeField(null=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Server(OrderedModel):
    tournament = models.ForeignKey(Tournament, on_delete=models.PROTECT)
    name = models.CharField(max_length=64)
    active = models.BooleanField(help_text='Pull records from this server')
    published = models.BooleanField(help_text='Publish information about this server on the public pages')
    location_country = models.CharField(max_length=2, blank=True)
    location = models.CharField(max_length=64, blank=True)
    domain_name = models.CharField(max_length=64, blank=True)
    address = models.GenericIPAddressField(protocol='IPv4')
    address_v6 = models.GenericIPAddressField(protocol='IPv6', blank=True, null=True)
    port = models.PositiveIntegerField(default=26000)
    server_db_url = models.CharField(max_length=1024)
    illegal_surveliance_url = models.CharField(max_length=1024, blank=True)

    def find_ip_for_crypto_idfp(self, crypto_idfp):
        if not self.illegal_surveliance_url:
            return
        resp = requests.get(self.illegal_surveliance_url)
        if resp.status_code != 200:
            logger.warning('Error during pulling packer %s %s', resp.status_code, resp.text)
            return
        ips = set()
        for i in resp.iter_lines():
            s = i.decode()
            if s.endswith('{}@Xon//Ks,'.format(crypto_idfp)):
                ip_port, _ = s.split(' ', 1)
                ips.add(ip_port.rsplit(':', 1)[0])
        return ips

    class Meta(OrderedModel.Meta):
        pass

    def __str__(self):
        return self.name


class Map(models.Model):
    tournament = models.ForeignKey(Tournament, on_delete=models.PROTECT)
    name = models.CharField(max_length=32)
    xonotic_name = models.CharField(max_length=32, db_index=True)
    active = models.BooleanField(default=False, help_text='Pull new results from server databases')
    thumbnail_url = models.URLField(null=True, blank=True)
    thumbnail_url_db_pixel = models.ImageField(null=True, blank=True, upload_to='map_pics')
    thumbnail_url_db_normal = models.ImageField(null=True, blank=True, upload_to='map_pics')
    schedule_start = models.DateTimeField()
    schedule_end = models.DateTimeField()
    youtube_url = models.CharField(blank=True, null=True, max_length=64)
    download_url = models.URLField(blank=True)
    mapper_players = models.ManyToManyField('xdwc.Player')
    compute_scores = models.BooleanField(default=True)

    def get_thumbnail_url(self):
        if self.thumbnail_url:
            return self.thumbnail_url
        else:
            now = timezone.now()
            if self.schedule_start < now:
                return os.path.join(settings.MEDIA_URL, self.thumbnail_url_db_normal.url)
            else:
                return os.path.join(settings.MEDIA_URL, self.thumbnail_url_db_pixel.url)

    @property
    def youtube_embed_code(self):
        if self.youtube_url:
            if '=' in self.youtube_url:
                return self.youtube_url.rsplit('=', 1)[-1]
            elif 'youtu.be/' in self.youtube_url:
                return self.youtube_url.rsplit('/', 1)[-1]
            else:
                return self.youtube_url
        return None

    def __str__(self):
        return self.xonotic_name

    def worldtimebuddy_widget_code(self):
        duration_sec = (self.schedule_end - self.schedule_start).total_seconds()
        duration_hours = math.floor(duration_sec / 3600)
        duration_mins = math.floor((duration_sec - duration_hours * 3600) / 60)
        return (
            """<span class="wtb-ew-v1" style="width: 560px; display:inline-block"><script src="https://www.worldtimebuddy.com/event_widget.js?h=100&md={}/{}/{}&mt={}.{}&ml={}.{}&sts=0&sln=0&wt=ew-ltc"></script><i><a target="_blank" href="https://www.worldtimebuddy.com/">Time converter</a> at worldtimebuddy.com</i><noscript><a href="https://www.worldtimebuddy.com/">Time converter</a> at worldtimebuddy.com</noscript><script>window[wtb_event_widgets.pop()].init()</script></span>"""
        ).format(
            self.schedule_start.month,
            self.schedule_start.day,
            self.schedule_start.year,
            self.schedule_start.hour,
            self.schedule_start.minute,
            duration_hours,
            duration_mins * 100 // 60
        )

    class Meta:
        ordering = ['schedule_start']


class PlayerKeyManager(models.Manager):
    def get_or_create_player(self, crypto_idfp, nickname):
        try:
            player_key = self.get(crypto_idfp=crypto_idfp)
            player = player_key.player
            if player.update_nickname(nickname):
                player.save()
        except PlayerKey.DoesNotExist:
            player = Player()
            player.update_nickname(nickname)
            player.save()
            player_key = self.create(crypto_idfp=crypto_idfp, player=player)
        return player_key, player


class PlayerKey(models.Model):
    crypto_idfp = models.CharField(max_length=128, unique=True)
    player = models.ForeignKey('xdwc.Player', related_name='keys', on_delete=models.CASCADE)

    objects = PlayerKeyManager()


class Player(models.Model):
    stats_id = models.PositiveIntegerField(null=True, blank=True)
    country = models.CharField(max_length=10, db_index=True, blank=True, null=True, default=None)
    is_banned = models.BooleanField(default=False)
    nickname = models.CharField(max_length=512)
    nickname_nocolors = models.CharField(max_length=512)
    nickname_html = models.TextField()
    force_nickname = models.CharField(
        max_length=512, blank=True,
        help_text='If not empty, nickname will be never pulled from the server, only this one will be used')
    admin_notes = models.TextField(blank=True)

    def merge(self, other_player):
        other_player.keys.all().update(player=self)
        other_player.speedrecord_set.update(player=self)
        cur_records = self.timerecord_set.all()
        best = None
        if cur_records.count() > 0:
            best = cur_records[0].time
        maps = set()
        for tr in other_player.timerecord_set.all():
            tr.player = self
            maps.add(tr.map)
            if tr.time < best:
                self.update_nickname(other_player.nickname)
                self.save()
                best = tr.time
            tr.save()
        for i in maps:
            Standings.objects.update_standings(i)
        other_player.delete()

    def scan_for_countries(self, geoip, surveillance_data):
        ips = set()
        for key in self.keys.all():
            for s in surveillance_data:
                l = s.strip()
                if l.endswith(f'{key.crypto_idfp}@Xon//Ks,') or l.endswith(f'{key.crypto_idfp}@~Xon//Ks,'):
                    ip_port, _ = s.split(' ', 1)
                    ip = ip_port.rsplit(':', 1)[0]
                    if ip.startswith('['):
                        ip = ip[1:-1]
                    ips.add(ip)
                ips = ips.union(ips)
        countries = set()
        for i in ips:
            resp = geoip.city(i)
            countries.add(resp.country.iso_code.lower())
        if not self.country and len(countries) == 1:
            self.country = countries.pop()
            self.save()
            print('Set country of %s to %s' % (self.nickname_nocolors, self.country))
        else:
            if self.country not in countries or len(countries) > 1:
                if len(ips) > 0:
                    print('Player %s has country %r but has the following geoloc info %s, ips %s' %
                          (self.nickname_nocolors, self.country, countries, ips))

    def get_map_history(self, map, best):
        bottom_margin = best * Decimal('1.5')
        time_records = map.timerecord_set.filter(player=self, time__lte=bottom_margin).order_by('timestamp')
        if time_records.count() == 0:
            return
        cur = time_records[0]
        res = [(cur.timestamp, best - cur.time, cur.time)]
        for i in time_records[1:]:
            if i.time < cur.time:
                res.append((i.timestamp, best - i.time, i.time))
                cur = i
        end_ts = min((timezone.now(), map.schedule_end))
        res.append((end_ts, best - cur.time, cur.time))
        return res

    def get_time_record_on_map(self, map):
        qs = map.timerecord_set.filter(player=self).order_by('time')
        if qs.count() > 0:
            return qs[0]

    def update_nickname(self, new_nickname):
        if self.force_nickname:
            new_nickname = self.force_nickname
        if self.nickname != new_nickname:
            self.nickname = new_nickname
            self.nickname_nocolors = strip_colors(
                qfont_decode(self.nickname, glyph_translation=True).replace('^^', '^'))
            self.nickname_html = html_colors(self.nickname)
            return True
        return False

    def __str__(self):
        return self.nickname_nocolors


class TimeRecordManager(models.Manager):
    def add_new_record(self, server, map, crypto_idfp, nickname, time):
        _, player = PlayerKey.objects.get_or_create_player(crypto_idfp, nickname)
        previous_records = self.filter(map=map, player=player).order_by('time')
        if previous_records.count() == 0 or (previous_records[0].time > time):
            return self.create(server=server, map=map, player=player, time=time)


class TimeRecord(models.Model):
    server = models.ForeignKey(Server, on_delete=models.PROTECT)
    map = models.ForeignKey(Map, on_delete=models.PROTECT)
    player = models.ForeignKey(Player, on_delete=models.PROTECT)
    time = models.DecimalField(max_digits=20, decimal_places=2)
    timestamp = models.DateTimeField(default=timezone.now)
    youtube_url = models.CharField(null=True, blank=True, max_length=64)

    objects = TimeRecordManager()

    def get_background_gradient(self):
        delta = timezone.now() - self.timestamp
        delta_seconds = delta.total_seconds()
        base_color = (34, 34, 34)
        target_color = (34, 190, 34)
        hilite_period = 24 * 60 * 60
        if delta_seconds >= hilite_period:
            res = base_color
        else:
            res = [
                base_color[0] + (target_color[0] - base_color[0]) * (1 - delta_seconds / hilite_period),
                base_color[1] + (target_color[1] - base_color[1]) * (1 - delta_seconds / hilite_period),
                base_color[2] + (target_color[2] - base_color[2]) * (1 - delta_seconds / hilite_period),
            ]
        return 'rgb({},{},{})'.format(*res)

    @property
    def timestamp_unix(self):
        return time.mktime(self.timestamp.timetuple())

    def __str__(self):
        return f'{self.player}: {self.map} - {self.time}'

    class Meta:
        ordering = ['time', 'timestamp']


class SpeedRecordManager(models.Manager):
    def add_new_record(self, map, server, crypto_idfp, nickname, speed):
        _, player = PlayerKey.objects.get_or_create_player(crypto_idfp, nickname)
        previous_records = self.filter(server=server, map=map).order_by('-speed')
        if previous_records.count() == 0 or (previous_records[0].speed < speed):
            return self.create(server=server, map=map, player=player, speed=speed)


class SpeedRecord(models.Model):
    server = models.ForeignKey(Server, on_delete=models.PROTECT)
    map = models.ForeignKey(Map, on_delete=models.PROTECT)
    player = models.ForeignKey(Player, on_delete=models.PROTECT)
    speed = models.DecimalField(max_digits=40, decimal_places=10)
    timestamp = models.DateTimeField(default=timezone.now)

    objects = SpeedRecordManager()

    def __str__(self):
        return f'Speed record on {self.map} by {self.player}'

    class Meta:
        ordering = ['-speed']


class StandingsManager(models.Manager):
    def __original_eesystem(self, pos, time, first_time):
        if pos == 1:
            return 1, settings.EESYSTEM_POS1_POINTS
        else:
            c1 = first_time / time
            c2 = calculate_c2(pos)
            return c1, c1 * (c2 / 100) * settings.EESYSTEM_POS1_POINTS

    def __revised_eesystem(self, pos, time, first_time, second_time):
        if second_time is None:
            return settings.EESYSTEM_POS1_POINTS
        if pos == 1:
            c1 = second_time / time
        else:
            c1 = first_time / time
        c2 = decimal.Decimal(settings.EESYSTEM_REVISED_C2_BASE ** (pos - 1))
        return c1, settings.EESYSTEM_POS1_POINTS * c1 * c2

    def update_standings(self, map):
        self.filter(time_record__map=map).delete()
        records = []
        for player in Player.objects.all():
            r = player.get_time_record_on_map(map)
            if r:
                records.append(r)
        records.sort(key=lambda x: x.time)
        pos = 0
        dup_number = 0
        prev_record = None
        for r in records:
            if r.player.is_banned:
                continue
            if prev_record is None or r.time != prev_record.time:
                pos += dup_number + 1
                dup_number = 0
            else:
                dup_number += 1
            prev_record = r
            if map.tournament.scoring_system == ScoringSystem.ORIGINAL_EESYSTEM:
                reltime, points = self.__original_eesystem(pos, r.time, records[0].time)
            elif map.tournament.scoring_system == ScoringSystem.REVISED_EESYSTEM:
                if len(records) > 1:
                    second_time = records[1].time
                else:
                    second_time = None
                reltime, points = self.__revised_eesystem(pos, r.time, records[0].time, second_time)
            else:
                raise NotImplementedError("Scoring system {} not implemented".format(map.tournament.scoring_system))
            if not map.compute_scores:
                points = decimal.Decimal(0)
            self.create(
                position=pos,
                time_record=r,
                eesystem_points=points,
                reltime=reltime,
                difftime=r.time - records[0].time
            )


class Standings(models.Model):
    position = models.PositiveIntegerField()
    time_record = models.ForeignKey(TimeRecord, on_delete=models.CASCADE)
    eesystem_points = models.DecimalField(max_digits=40, decimal_places=10)
    reltime = models.DecimalField(max_digits=40, decimal_places=10, default=0)
    difftime = models.DecimalField(max_digits=40, decimal_places=10, default=0)

    @property
    def eesystem_points_rounded(self):
        return self.eesystem_points.to_integral_exact(rounding=decimal.ROUND_HALF_UP)

    objects = StandingsManager()

    class Meta:
        ordering = ['position']


class TotalStandingsManager(models.Manager):
    def update_standings(self, tournament):
        players = {}
        maps = list(Map.objects.filter(tournament=tournament))
        for m in maps:
            standings = Standings.objects.select_related(
                'time_record', 'time_record__player', 'time_record__map'
            ).filter(time_record__map=m)
            for i in standings:
                player = i.time_record.player
                if player not in players:
                    players[player] = Decimal(0)
                players[player] += i.eesystem_points

        for player, score in players.items():
            try:
                standings = self.get(player=player, tournament=tournament)
                if score != standings.score:
                    standings.score = score
                    standings.save()
            except self.model.DoesNotExist:
                self.create(player=player, tournament=tournament, score=score)


class TotalStandings(models.Model):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    score = models.DecimalField(max_digits=40, decimal_places=10)

    objects = TotalStandingsManager()


class Rules(models.Model):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    text = RichTextField()

    def __str__(self):
        return str(self.tournament)

# @receiver(post_save, sender=TimeRecord)
# def update_standings(instance, **kwargs):
#     print(instance.map)
#     Standings.objects.update_standings(instance.map)
