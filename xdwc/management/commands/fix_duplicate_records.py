import logging
import geoip2.database
import requests

from django.conf import settings
from django.core.management.base import BaseCommand

from xdwc.models import Map, Standings, Player, Server, TimeRecord

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Rescans player countries'

    def handle(self, *args, **kwargs):
        num_deleted = 0
        for p in Player.objects.all():
            for s in Server.objects.all():
                cur = None
                for tr in TimeRecord.objects.filter(player=p, server=s):
                    if cur is not None and cur.time == tr.time:
                        tr.delete()
                        num_deleted += 1
                    else:
                        cur = tr
        print('Deleted %d records' % num_deleted)