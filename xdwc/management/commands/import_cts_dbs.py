import logging

import requests
from django.core.management.base import BaseCommand, CommandError

from xdwc.models import Server, Map, Standings, TotalStandings
from xdwc.server_db import ServerDB, BrokenDBError

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Imports new records for active maps from server databases'

    def handle(self, *args, **kwargs):
        tournaments = set()
        for map in Map.objects.select_related('tournament').filter(active=True):
            for server in Server.objects.filter(tournament=map.tournament, active=True):
                if server.server_db_url.startswith('file://'):
                    path = server.server_db_url[len('file://'):]
                    try:
                        data = open(path, 'r').read()
                    except (IOError, OSError):
                        logger.warning('Could now read server db for server %s', server.name, exc_info=True)
                        continue
                else:
                    try:
                        resp = requests.get(server.server_db_url)
                        if resp.status_code != 200:
                            logger.warning(
                                'Could now download server db for server %s: %s',
                                server.name,
                                resp.status_code
                            )
                            continue
                        data = resp.text
                    except Exception as e:
                        logger.warning('Exception during downloading server db for server %s: %s', server.name, e)
                        continue
                server_db = ServerDB(data)
                try:
                    server_db.process_map(server, map)
                except BrokenDBError:
                    logger.warning('Broken DB for server %s', server.name, exc_info=True)
            Standings.objects.update_standings(map)
            tournaments.add(map.tournament)
        for i in tournaments:
            TotalStandings.objects.update_standings(i)
