from io import StringIO
import csv
from django.core.management.base import BaseCommand, CommandError

from xdwc.models import Map, TimeRecord, Standings


class Command(BaseCommand):
    help = 'Dumps csv for video rendering'

    def add_arguments(self, parser):
        parser.add_argument('map_id', type=int)

    def handle(self, *args, **kwargs):
        map = Map.objects.get(id=kwargs['map_id'])
        stringio = StringIO()
        writer = csv.writer(stringio)
        for i in Standings.objects.filter(time_record__map=map):
            writer.writerow(
                [
                    i.position,
                    i.time_record.player.nickname_nocolors,
                    i.time_record.player.country,
                    i.time_record.time,
                    i.eesystem_points,
                    i.reltime,
                    i.difftime,
                    ';'.join([j.crypto_idfp for j in i.time_record.player.keys.all()])
                ]
            )
        print(stringio.getvalue())
