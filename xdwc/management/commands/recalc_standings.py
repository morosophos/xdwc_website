import logging

from django.core.management.base import BaseCommand

from xdwc.models import Map, Standings, Tournament, TotalStandings

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Recalculates standings'

    def handle(self, *args, **kwargs):
        for map in Map.objects.select_related('tournament').all():
            Standings.objects.update_standings(map)
        for tournament in Tournament.objects.all():
            TotalStandings.objects.update_standings(tournament)
