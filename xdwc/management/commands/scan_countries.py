import logging
import sqlite3
from io import BytesIO

import geoip2.database
import requests

from django.conf import settings
from django.core.management.base import BaseCommand

from xdwc.models import Map, Standings, Player, Server

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Rescans player countries'

    def handle(self, *args, **kwargs):
        geoip = geoip2.database.Reader(settings.GEOIP_DB_PATH)
        surveillance_data = []
        for i in Server.objects.all():
            resp = requests.get(i.illegal_surveliance_url)
            if resp.status_code != 200:
                print('Error during pulling packer %s %s' % (resp.status_code, resp.text))
                return
            db = sqlite3.connect(BytesIO(resp.content))
            surveillance_data += resp.text.splitlines()
        for player in Player.objects.all():
            player.scan_for_countries(geoip, surveillance_data)
