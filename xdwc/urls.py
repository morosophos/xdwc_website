from django.urls import *

from .views import *

app_name = 'xdwc'

tournament_urls = ([
    path('', IndexView.as_view(), name='index'),
    path('standings.json/<int:base_time>', StandingsJsonView.as_view(), name='standings-json'),
    path('map/<map_name>/', MapView.as_view(), name='map'),
    path('schedule/', MapsView.as_view(), name='maps'),
    path('rules/', RulesView.as_view(), name='rules'),
    path('servers/', ServersView.as_view(), name='servers')
], 'tournament')

urlpatterns = [
    path('player/<int:player_id>/', PlayerView.as_view(), name='player'),
    path('mappic/<int:map_id>', MapThumbnailImage.as_view(), name='map_thumbnail'),
    path('', IndexView.as_view(), {'tournament': None}),
    path('xqc91/', include(tournament_urls, namespace='xqc91'), {'tournament': 'xqc91'}),
    path('xdwc2018/', include(tournament_urls, namespace='xdwc2018'), {'tournament': 'xdwc2018'}),
    path('xdwc2019/', include(tournament_urls, namespace='xdwc2019'), {'tournament': 'xdwc2019'}),
    path('xdwc2020/', include(tournament_urls, namespace='xdwc2020'), {'tournament': 'xdwc2020'}),
    path('xdwc2021/', include(tournament_urls, namespace='xdwc2021'), {'tournament': 'xdwc2021'}),
    path('xdwc2022/', include(tournament_urls, namespace='xdwc2022'), {'tournament': 'xdwc2022'}),
    path('xdwc2023/', include(tournament_urls, namespace='xdwc2023'), {'tournament': 'xdwc2023'}),
    path('xdwc2024/', include(tournament_urls, namespace='xdwc2024'), {'tournament': 'xdwc2024'}),
]
