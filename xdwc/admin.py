from django.contrib import admin, messages
from django.contrib.admin.models import LogEntry
from django.urls import reverse
from django.utils.safestring import mark_safe
from ordered_model.admin import OrderedModelAdmin

from .models import Tournament, Map, Server, TimeRecord, SpeedRecord, Player, Rules, PlayerKey, Standings


class TournamentAdmin(admin.ModelAdmin):
    pass


class MapAdmin(admin.ModelAdmin):
    list_display = ['xonotic_name', 'active', 'schedule_start', 'schedule_end']


class ServerAdmin(OrderedModelAdmin):
    list_display = ['name', 'tournament', 'active', 'published', 'move_up_down_links']
    list_filter = ['tournament']


class TimeRecordAdmin(admin.ModelAdmin):
    list_display = ['server', 'map', 'player', 'time', 'timestamp']
    list_filter = ['map', 'server', 'player']


class SpeedRecordAdmin(admin.ModelAdmin):
    list_display = ['server', 'map', 'player', 'speed', 'timestamp']
    list_filter = ['map', 'server', 'player']


class PlayerKeyInline(admin.TabularInline):
    model = PlayerKey


class PlayerAdmin(admin.ModelAdmin):
    def merge(self, request, queryset):
        queryset = queryset.order_by('id')
        if queryset.count() < 2:
            self.message_user(request, 'Please select two or more players to merge', messages.ERROR)
            return
        base = queryset[0]
        for i in queryset[1:]:
            base.merge(i)
            self.message_user(request, '%s merged into %s' % (i.nickname_nocolors, base.nickname_nocolors))

    merge.short_description = 'Merge two or more players'
    actions = ['merge']
    list_display = ['nickname_nocolors', 'country', 'stats_id', 'admin_notes']
    list_filter = ['country']
    inlines = [PlayerKeyInline]
    list_per_page = 1000


class RulesAdmin(admin.ModelAdmin):
    pass


class LogEntryAdmin(admin.ModelAdmin):
    def object_url(self, obj):
        o = obj.get_edited_object()
        if o is None:
            return
        return mark_safe('<a href="{}">{}</a>'.format(
            reverse(f'admin:{obj.content_type.app_label}_{obj.content_type.model}_change', args=(o.id,)),
            o))

    list_display = ['user', 'action_time', '__str__', 'object_url']
    list_filter = ['user']


class StandingsAdmin(admin.ModelAdmin):
    def player(self, obj):
        return obj.time_record.player

    def map(self, obj):
        return obj.time_record.map

    def time(self, obj):
        return obj.time_record.time

    list_display = ['position', 'player', 'map', 'time', 'eesystem_points']
    list_filter = ['time_record__player', 'time_record__map']


admin.site.register(Tournament, TournamentAdmin)
admin.site.register(Map, MapAdmin)
admin.site.register(Server, ServerAdmin)
admin.site.register(TimeRecord, TimeRecordAdmin)
admin.site.register(SpeedRecord, SpeedRecordAdmin)
admin.site.register(Player, PlayerAdmin)
admin.site.register(Rules, RulesAdmin)
admin.site.register(LogEntry, LogEntryAdmin)
admin.site.register(Standings, StandingsAdmin)
