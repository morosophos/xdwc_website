from decimal import Decimal

def calculate_c2(pos):
    order = pos // 50
    subpos = pos - order * 50
    base = Decimal(100) / 2 ** order
    mul = Decimal(1) / 2 ** order
    return base - subpos * mul
