import datetime
import math
import time
from datetime import datetime
from decimal import Decimal, ROUND_HALF_UP

from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.utils import timezone
from django.utils.timezone import make_aware
from django.views import View
from django.http import JsonResponse, HttpResponse

from xdwc.models import Map, Standings, SpeedRecord, Rules, Tournament, Server, Player, TimeRecord, TotalStandings
from xdwc.templatetags.xdwc import player_name, format_time
from xdwc.util import html_colors


class StandingsJsonView(View):
    def get(self, request, tournament, base_time):
        players = {}
        # base_time = float(base_time)
        # base_dt = make_aware(
        #     datetime.datetime.fromtimestamp(base_time)
        # )
        if tournament is None:
            tournament = get_object_or_404(Tournament.objects, is_default=True)
        else:
            tournament = get_object_or_404(Tournament.objects, slug=tournament)
        total_standings = dict([(i.player, i) for i in TotalStandings.objects.select_related(
            'player'
        ).filter(
            tournament=tournament
        )])
        standings = Standings.objects.select_related(
            'time_record', 'time_record__player', 'time_record__map'
        ).filter(
            time_record__map__tournament=tournament,
            # time_record__timestamp__gte=base_dt
        )
        for i in standings:
            player = i.time_record.player
            if player not in players:
                players[player] = {
                    'maps': {},
                    'total': total_standings[player].score
                }
            players[player]['maps'][i.time_record.map] = i
        for player, score in total_standings.items():
            if player not in players:
                players[player] = {
                    'maps': {},
                    'total': score.score
                }
        players_list = []
        for i in players:
            players_list.append(
                {
                    'id': i.id,
                    'name': player_name(i),
                    'maps': dict(
                        [(map.xonotic_name, {
                            'time': int(rec.time_record.time * 100),
                            'position': rec.position,
                            'score': int(rec.eesystem_points * 100),
                            'timestamp': rec.time_record.timestamp_unix,
                            'time_formatted': format_time(rec.time_record.time)
                        }) for (map, rec) in players[i]['maps'].items()]
                    ),
                    'total': {
                        'score': int(players[i]['total'] * 1000000000),
                        'score_formatted': str(
                            players[i]['total'].quantize(
                                Decimal('.01'),
                                rounding=ROUND_HALF_UP)
                        )
                    }
                }
            )
        return JsonResponse({
            'players': players_list,
            'current_time': time.time()
        }, status=200)


class IndexView(View):
    def get(self, request, tournament):
        players = {}
        if tournament is None:
            tournament = get_object_or_404(Tournament.objects, is_default=True)
        else:
            tournament = get_object_or_404(Tournament.objects, slug=tournament)
        maps = list(Map.objects.filter(tournament=tournament))
        for m in maps:
            standings = Standings.objects.select_related(
                'time_record', 'time_record__player', 'time_record__map'
            ).filter(time_record__map=m)
            for i in standings:
                player = i.time_record.player
                if player not in players:
                    players[player] = {
                        'maps': {},
                        'total': 0
                    }
                players[player]['total'] += i.eesystem_points
                players[player]['maps'][m] = i

        if request.GET.get('ordering') is None or request.GET['ordering'] == 'total':
            players = dict(sorted([(k, v) for k, v in players.items()], key=lambda x: x[1]['total'], reverse=True))
        else:
            sort_by_map = get_object_or_404(Map.objects, xonotic_name=request.GET['ordering'])
            def get_map_points(v):
                m = v[1]['maps'].get(sort_by_map)
                if m:
                    return m.eesystem_points
                else:
                    return 0
            players = dict(sorted([(k, v) for k, v in players.items()], key=get_map_points, reverse=True))
        qs = Map.objects.filter(schedule_start__gt=timezone.now(),
                                tournament=tournament).order_by('schedule_start')
        next_map = None
        last_map = None
        if qs.count() > 0:
            next_map = qs[0]
        else:
            current_map = Map.objects.filter(
                schedule_start__lt=timezone.now(),
                schedule_end__gt=timezone.now()
            )
            if current_map.count() > 0:
                last_map = current_map[0]

        return render(request, 'xdwc/index.jinja', {
            'players': players,
            'maps': maps,
            'ordering': request.GET.get('ordering') or 'total',
            'current_nav_tab': 'index',
            'next_map': next_map,
            'last_map': last_map,
            'tournament': tournament,
            'fluid_layout': False
        })


class MapsView(View):
    def get(self, request, tournament):
        if tournament is None:
            tournament = get_object_or_404(Tournament.objects, is_default=True)
        else:
            tournament = get_object_or_404(Tournament.objects, slug=tournament)
        maps = Map.objects.filter(tournament=tournament)\
            .prefetch_related('mapper_players')\
            .exclude(schedule_start=None)\
            .exclude(schedule_end=None)\
            .order_by('schedule_start')
        now = timezone.now()
        return render(request, 'xdwc/maps.jinja', {
            'maps': maps,
            'now': now,
            'current_nav_tab': 'maps',
            'tournament': tournament,
            'html_colors': html_colors
        })


class MapView(View):
    def get(self, request, map_name, tournament):
        if tournament is None:
            tournament = get_object_or_404(Tournament.objects, is_default=True)
        else:
            tournament = get_object_or_404(Tournament.objects, slug=tournament)
        map = get_object_or_404(
            Map.objects.prefetch_related('mapper_players'),
            xonotic_name=map_name,
            tournament=tournament
        )
        try:
            prev_map = map.get_previous_by_schedule_start(tournament=tournament)
        except Map.DoesNotExist:
            prev_map = None
        try:
            next_map = map.get_next_by_schedule_start(tournament=tournament)
        except Map.DoesNotExist:
            next_map = None
        speed_records = SpeedRecord.objects.select_related('player').filter(map=map, player__is_banned=False)
        standings = Standings.objects.select_related(
            'time_record', 'time_record__player', 'time_record__map'
        ).prefetch_related('time_record__map__timerecord_set').filter(time_record__map=map)
        history = {}
        bottom_margin = None
        c = standings.count()
        best_time = worst_time = 0
        if c > 0:
            best = standings[0].time_record.time
            bottom_margin = best - best * Decimal('1.20')
            for i in standings:
                h = i.time_record.player.get_map_history(map, best)
                if h is not None:
                    history[i.time_record.player] = h
                worst_time = i.time_record.time
            best_time = standings[0].time_record.time

        return render(request, 'xdwc/map.jinja', {
            'tournament': tournament,
            'speed_records': speed_records[:1],
            'standings': standings,
            'map_name': map_name,
            'map': map,
            'current_nav_tab': 'index',
            'history': history,
            'bottom_margin': bottom_margin,
            'worst_time': worst_time,
            'best_time': best_time,
            'prev_map': prev_map,
            'next_map': next_map,
            'html_colors': html_colors,
        })


class RulesView(View):
    def get(self, request, tournament):
        if tournament is None:
            tournament = get_object_or_404(Tournament.objects, is_default=True)
        else:
            tournament = get_object_or_404(Tournament.objects, slug=tournament)
        rules = get_list_or_404(Rules.objects, tournament=tournament)
        return render(request, 'xdwc/rules.jinja', {
            'rules': rules[0].text,
            'current_nav_tab': 'rules',
            'tournament': tournament
        })


class ServersView(View):
    def get(self, request, tournament):
        if tournament is None:
            tournament = get_object_or_404(Tournament.objects, is_default=True)
        else:
            tournament = get_object_or_404(Tournament.objects, slug=tournament)
        servers = Server.objects.filter(tournament=tournament, published=True)
        return render(request, 'xdwc/servers.jinja', {
            'tournament': tournament,
            'servers': servers,
            'current_nav_tab': 'servers'
        })


class PlayerView(View):
    def get(self, request, player_id):
        player = get_object_or_404(Player.objects, id=player_id, is_banned=False)
        tournament = Tournament.objects.get(is_default=True)
        time_records = list(
            Standings.objects.select_related(
                'time_record', 'time_record__map', 'time_record__map__tournament'
            ).filter(time_record__player=player).order_by('-time_record__timestamp')
        )
        # anyone cares about speed records? meh
        # speed_records = list(
        #     SpeedRecord.objects.select_related('map').filter(player=player).order_by('-timestamp')
        # )
        return render(request, 'xdwc/player.jinja', {
            'player': player,
            'tournament': tournament,
            'time_records': time_records,
        })


class MapThumbnailImage(View):
    def get(self, request, map_id):
        now = datetime.utcnow()
        map = get_object_or_404(Map.objects, id=map_id)
        if map.schedule_start > now:
            return HttpResponse(map.thumbnail_url_db_normal, content_type='image/jpeg')
        else:
            return HttpResponse(map.thumbnail_url_db_pixel, content_type='image/jpeg')
